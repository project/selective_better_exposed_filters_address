<?php

namespace Drupal\selective_better_exposed_filters_address\Plugin\better_exposed_filters\filter;

use Drupal\address\Plugin\views\filter\CountryAwareInOperatorBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Psuedo-dynamic object inhertitance.
 *
 * @see https://stackoverflow.com/a/16773369
 */
if (class_exists('\Drupal\selective_better_exposed_filters\Plugin\better_exposed_filters\filter\DefaultWidget')) {
  class DynamicDefaultWidgetBase extends \Drupal\selective_better_exposed_filters\Plugin\better_exposed_filters\filter\DefaultWidget {}
}
else {
  class DynamicDefaultWidgetBase extends \Drupal\better_exposed_filters\Plugin\better_exposed_filters\filter\DefaultWidget {}
}


/**
 * Default widget implementation.
 *
 * @BetterExposedFiltersFilterWidget(
 *   id = "default",
 *   label = @Translation("Default"),
 * )
 */
class DefaultWidget extends DynamicDefaultWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + SelectiveFilterBase::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    $filter = $this->handler;
    $form = parent::buildConfigurationForm($form, $form_state);
    $form += SelectiveFilterBase::buildConfigurationForm($filter, $this->configuration);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    SelectiveFilterBase::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $form_state): void {
    parent::exposedFormAlter($form, $form_state);
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    $filter = $this->handler;
    if ($filter instanceof CountryAwareInOperatorBase) {
      SelectiveFilterBase::exposedFormAlter($this->view, $filter, $this->configuration, $form, $form_state);
    }
  }

}
