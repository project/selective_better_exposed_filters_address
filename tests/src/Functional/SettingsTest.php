<?php

declare(strict_types=1);

namespace Drupal\Tests\selective_better_exposed_filters_address\Functional;

use Drupal\Tests\BrowserTestBase;
use PHPUnit\Framework\Attributes\Group;

/**
 * Settings test.
 */
#[Group('selective_better_exposed_filters_address')]
final class SettingsTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['views_ui'];

  /**
   * {@inheritdoc}
   */
  public static $testViews = ['sbefa__test'];

  protected function setUp($import_test_views = TRUE, $modules = ['views_test_config']): void {
    parent::setUp($import_test_views, $modules);
    // Create and login admin user.
    $this->adminUser = $this->drupalCreateUser([
      'administer views'
    ]);
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/structure/views/nojs/display/sbefa__test/disabled/exposed_form_options');
  }

  /**
   * Test address field country code settings.
   */
  public function testAddressCountryCode(): void {
    $edit = [
      'exposed_form_options[bef][filter][field_address_country_code][configuration][options_show_only_used_filtered_address]' => TRUE,
    ];
    $this->submitForm($edit, 'Apply');
    $this->assert
      ->statusMessageContains('Required field, if "Filter address values based on filtered result set" enabled', 'error');
  }

  /**
   * Test address field administrative area settings.
   */
  public function testAddressAdministrativeAreaSettings(): void {
    $edit = [
      'exposed_form_options[bef][filter][field_address_administrative_area][configuration][options_show_only_used_filtered_address]' => TRUE,
    ];
    $this->submitForm($edit, 'Apply');
    $this->assert
      ->statusMessageContains('Required field, if "Filter address values based on filtered result set" enabled', 'error');
  }
}
