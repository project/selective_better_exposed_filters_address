<?php

declare(strict_types=1);

namespace Drupal\Tests\selective_better_exposed_filters_address\FunctionalJavascript;

use Drupal\Tests\selective_better_exposed_filters_address\Traits\TestDataTrait;
use PHPUnit\Framework\Attributes\Group;

/**
 * Tests the JavaScript functionality of the Selective Better Exposed Filters for Address module.
 */
#[Group('selective_better_exposed_filters_address')]
final class SettingsTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static $testViews = ['sbefa__test'];

  protected function setUp(): void {
    parent::setUp();
    $this->drupalGet('/admin/structure/views/view/sbefa__test/edit/disabled');
    $this->page
      ->findById('edit-displays-settings-settings-content-tab-content-details-columns-third')
      ->click();
    $this->page
      ->findById('views-disabled-exposed-form-options')
      ->click();
    $this->assert
      ->assertWaitOnAjaxRequest();
  }

  /**
   * Test address field country code settings.
   */
  public function testAddressCountryCode(): void {
    $this->page
      ->find('css', 'details[data-drupal-selector="edit-exposed-form-options-bef-filter-field-address-country-code"]')
      ->click();
    $used_address = $this->page
      ->findField('exposed_form_options[bef][filter][field_address_country_code][configuration][options_show_only_used_address]');
    $used_filtered_address = $this->page
      ->findField('exposed_form_options[bef][filter][field_address_country_code][configuration][options_show_only_used_filtered_address]');
    $this->assertFalse($used_address->hasAttribute('disabled'));
    $this->assertTrue($used_filtered_address->hasAttribute('disabled'));
    // Click "Show only used address values" checkbox.
    $used_address->click();
    $this->assertFalse($used_filtered_address->hasAttribute('disabled'));
  }

  /**
   * Test address field administrative area settings.
   */
  public function testAddressAdministrativeAreaSettings(): void {
    $this->page
      ->find('css', 'details[data-drupal-selector="edit-exposed-form-options-bef-filter-field-address-administrative-area"]')
      ->click();
    $used_address = $this->page
      ->findField('exposed_form_options[bef][filter][field_address_administrative_area][configuration][options_show_only_used_address]');
    $used_filtered_address = $this->page
      ->findField('exposed_form_options[bef][filter][field_address_administrative_area][configuration][options_show_only_used_filtered_address]');
    $this->assertFalse($used_address->hasAttribute('disabled'));
    $this->assertTrue($used_filtered_address->hasAttribute('disabled'));
    // Click "Show only used address values" checkbox.
    $used_address->click();
    $this->assertFalse($used_filtered_address->hasAttribute('disabled'));
  }

}
