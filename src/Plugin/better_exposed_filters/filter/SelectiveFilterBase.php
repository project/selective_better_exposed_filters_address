<?php

namespace Drupal\selective_better_exposed_filters_address\Plugin\better_exposed_filters\filter;

use Drupal\address\Plugin\views\filter\AdministrativeArea;
use Drupal\address\Plugin\views\filter\CountryAwareInOperatorBase;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;

/**
 * Base class for Better exposed filters widget plugins.
 */
abstract class SelectiveFilterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultConfiguration(): array {
    return [
      'options_show_only_used_address' => FALSE,
      'options_show_only_used_filtered_address' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function buildConfigurationForm(FilterPluginBase $filter, array $settings): array {
    $form = [];
    if ($filter->isExposed() && $filter instanceof CountryAwareInOperatorBase) {
      $form['options_show_only_used_address'] = [
        '#type' => 'checkbox',
        '#title' => t('Show only used address values'),
        '#default_value' => !empty($settings['options_show_only_used_address']),
        '#description' => t('Restrict exposed filter values to those presented in the result set.'),
      ];

      $form['options_show_only_used_filtered_address'] = [
        '#type' => 'checkbox',
        '#title' => t('Filter address values based on filtered result set'),
        '#default_value' => !empty($settings['options_show_only_used_filtered_address']),
        '#description' => t('Restrict exposed filter values to those presented in the already filtered result set.'),
        '#states' => [
          'disabled' => [
            ':input[name="exposed_form_options[bef][filter][' . $filter->field . '][configuration][options_show_only_used_address]"]' => ['checked' => FALSE],
          ],
        ],
      ];
    }
    return $form;
  }

  /**
   * Configuration form validation.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public static function validateConfigurationForm(array &$form, FormStateInterface $form_state) : void {
    if (!$form_state->getValue('options_show_only_used_address') && $form_state->getValue('options_show_only_used_filtered_address')) {
      $form_state->setError($form['options_show_only_used_address'], t('Required field, if "Filter address values based on filtered result set" enabled.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function exposedFormAlter(ViewExecutable $current_view, FilterPluginBase $filter, array $settings, array &$form, FormStateInterface $form_state): void {
    if ($filter->isExposed() && !empty($settings['options_show_only_used_address']) && $settings['options_show_only_used_address'] == TRUE) {
      $identifier = $filter->options['is_grouped'] ? $filter->options['group_info']['identifier'] : $filter->options['expose']['identifier'];

      if (empty($current_view->selective_filter)) {
        $view = Views::getView($current_view->id());
        if ($view === NULL) {
          // error?
          return;
        }
        $view->selective_filter = TRUE;
        $view->setArguments($current_view->args);
        $view->setItemsPerPage(0);
        $view->setDisplay($current_view->current_display);
        $view->preExecute();
        $view->execute();

        if (!empty($view->result)) {
          $field_id = $filter->definition['field_name'] ?? NULL;
          $element = &$form[$identifier];

          // Switch between administrative area and country based on filter.
          if ($filter instanceof AdministrativeArea) {
            $column = 'administrative_area';
          }
          else {
            $column = 'country_code';
          }

          $allowed = [];
          foreach ($view->result as $row) {
            $entity = $row->_entity;
            if ($field_id && $entity instanceof FieldableEntityInterface && $entity->hasField($field_id)) {
              $addresses = $entity->get($field_id)->getValue();

              if (!empty($addresses)) {
                foreach ($addresses as $item) {
                  $value = $item[$column];
                  $allowed[$value] = $value;
                }
              }
            }
          }

          if (!empty($element['#options'])) {
            foreach ($element['#options'] as $key => $option) {
              if ($key == 'All') {
                continue;
              }

              $target_id = $key;
              if (is_object($option) && !empty($option->option)) {
                $target_id = array_keys($option->option);
                $target_id = reset($target_id);
              }
              if (!in_array($target_id, $allowed)) {
                unset($element['#options'][$key]);
              }
            }
          }
        }
      }
      else {
        if (!empty($settings['options_show_only_used_filtered_address'])) {
          $user_input = $form_state->getUserInput();
          if (isset($user_input[$identifier])) {
            unset($user_input[$identifier]);
          }
        }
        else {
          $user_input = [];
        }
        $form_state->setUserInput($user_input);
      }
    }
  }

}
