<?php

declare(strict_types=1);

namespace Drupal\Tests\selective_better_exposed_filters_address\Functional;

use Drupal\Tests\BrowserTestBase;
use PHPUnit\Framework\Attributes\Group;

/**
 * Disabled filter test.
 */
#[Group('selective_better_exposed_filters_address')]
final class DisabledFilterTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static $testViews = ['sbefa__test'];

  /**
   * Test disabled filter.
   */
  public function testDisabledFilter(): void {
    $this->drupalGet('/sbefa--test/disabled');
    $this->assert
      ->elementsCount('css', 'select[name="field_address_country_code"] option', count(\Drupal::service('address.country_repository')->getList()) + 1); // "- Any -
    $this->assert
      ->elementsCount('css', '.view-sbefa__test tbody tr', 14);
    $edit = [
      'field_address_country_code' => 'US'
    ];
    $this->submitForm($edit, 'Apply');
    $this->assert
      ->elementsCount('css', 'select[name="field_address_administrative_area"] option', 63);
  }
}
