# Selective Better Exposed Filters for Address

INTRODUCTION
------------
This module adds extra options to the Better Exposed Filters configuration page.
While very similar to the Selective Better Exposed Filters module which works
with taxonomy terms, this module works in conjunction with the Address module’s
administrative_area and country_code field values. There are options to restrict
exposed filter values to those presented in the result set or in the already
filtered result set.

REQUIREMENTS
------------

This module requires the following modules:
 * [Address](https://drupal.org/project/address)
 * [Better Exposed Filters](https://drupal.org/project/better_exposed_filters)

INSTALLATION
------------

It's very easy. You just need to:
 - Enable the module
 - Change settings of Better Exposed Filter in your view as usual

### Composer
If your site is [managed via Composer](https://www.drupal.org/node/2718229),
use Composer to download the module:
   ```sh
   composer require "drupal/selective_better_exposed_filters_address"
   ```

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will add a few options to the Better Exposed Filter settings
inside the View.


MAINTAINERS
-----------

Current maintainers:
 * Kurucz István (nevergone) (https://www.drupal.org/u/nevergone)
 * Daniel Korte (Daniel Korte) (https://www.drupal.org/u/daniel-korte)
