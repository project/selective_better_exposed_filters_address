<?php

namespace Drupal\Tests\selective_better_exposed_filters_address\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for selective_better_exposed_filters_address.
 *
 * @group selective_better_exposed_filters_address
 */
class GenericTest extends GenericModuleTestBase {}
