<?php declare(strict_types = 1);

namespace Drupal\Tests\selective_better_exposed_filters_address\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\selective_better_exposed_filters_address\Traits\TestDataTrait;
use Drupal\Tests\views\Functional\ViewTestBase;
use Drupal\views\Tests\ViewTestData;

/**
 * Base class for selective_better_exposed_filters_address tests.
 *
 * @group selective_better_exposed_filters_address
 */
abstract class TestBase extends ViewTestBase {

  use ContentTypeCreationTrait;
  use NodeCreationTrait;
  use TestDataTrait;

  /**
   * Set to TRUE to strict check all configuration saved.
   *
   * At the moment, we cannot fully validate the configuration, so the tests
   * will not run. It can be removed if apple allows you to use
   * getThirdPartySetting() in the configuration.
   *
   * @see \Drupal\Core\Config\Testing\ConfigSchemaChecker
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * @var \Drupal\user\Entity\User
   *
   * The admin user.
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'address',
    'field',
    'node',
    'better_exposed_filters',
    'selective_better_exposed_filters_address',
    'sbefa__test',
    'user',
    'views',
  ];

  /**
   * @var \Behat\Mink\Element\DocumentElement
   */
  protected $page;

  /**
   * @var \Drupal\Tests\WebAssert
   */
  protected $assert;

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = ['views_test_config']): void {
    parent::setUp(TRUE, ['sbefa__test']);
    $this->page = $this->getSession()->getPage();
    $this->assert = $this->assertSession();
    // Create content type and field.
    $this->createContentType([
      'type' => 'test_content_type',
      'name' => 'Test Content Type',
    ]);
    $field_storage = FieldStorageConfig::create([
      'entity_type' => 'node',
      'field_name' => 'field_address',
      'type' => 'address',
      'cardinality' => 1,
    ]);
    $field_storage->save();
    $field = [
      'field_name' => 'field_address',
      'label' => 'Adress',
      'entity_type' => 'node',
      'bundle' => 'test_content_type',
      'required' => TRUE,
    ];
    FieldConfig::create($field)->save();
    // Assign form settings.
    \Drupal::service('entity_display.repository')->getFormDisplay('node', 'test_content_type')
      ->setComponent('field_address', [
        'type' => 'address_default',
      ])
      ->save();
    // Assign display settings.
    \Drupal::service('entity_display.repository')->getViewDisplay('node', 'test_content_type')
      ->setComponent('field_address', [
        'type' => 'address_default',
      ])
      ->save();
    // Create and login admin user.
    $this->adminUser = $this->drupalCreateUser([
      'access content overview',
      'administer nodes',
      'create test_content_type content',
      'edit any test_content_type content',
    ]);
    $this->drupalLogin($this->adminUser);
    // Generate test contents.
    foreach ($this->getTestData() as $content) {
      $this->createNode($content + ['type' => 'test_content_type']);
    }
  }
}
