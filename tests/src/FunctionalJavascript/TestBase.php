<?php

declare(strict_types=1);

namespace Drupal\Tests\selective_better_exposed_filters_address\FunctionalJavascript;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\selective_better_exposed_filters_address\Traits\TestDataTrait;
use Drupal\views\Tests\ViewTestData;
use PHPUnit\Framework\Attributes\Group;

/**
 * Tests the JavaScript functionality of the Selective Better Exposed Filters for Address module.
 */
#[Group('selective_better_exposed_filters_address')]
abstract class TestBase extends WebDriverTestBase {

  use TestDataTrait;

  /**
   * Set to TRUE to strict check all configuration saved.
   *
   * At the moment, we cannot fully validate the configuration, so the tests
   * will not run. It can be removed if apple allows you to use
   * getThirdPartySetting() in the configuration.
   *
   * @see \Drupal\Core\Config\Testing\ConfigSchemaChecker
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'address',
    'field',
    'node',
    'better_exposed_filters',
    'selective_better_exposed_filters_address',
    'sbefa__test',
    'user',
    'views',
    'views_ui'
  ];

  /**
   * @var \Drupal\user\Entity\User
   *
   * The admin user.
   */
  protected $adminUser;

  /**
   * @var \Behat\Mink\Element\DocumentElement
   */
  protected $page;

  /**
   * @var \Drupal\FunctionalJavascriptTests\WebDriverWebAssert|\Drupal\Tests\WebAssert
   */
  protected $assert;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->page = $this->getSession()->getPage();
    $this->assert = $this->assertSession();
    // Create content type and field.
    $this->createContentType([
      'type' => 'test_content_type',
      'name' => 'Test Content Type',
    ]);
    $field_storage = FieldStorageConfig::create([
      'entity_type' => 'node',
      'field_name' => 'field_address',
      'type' => 'address',
      'cardinality' => 1,
    ]);
    $field_storage->save();
    $field = [
      'field_name' => 'field_address',
      'label' => 'Adress',
      'entity_type' => 'node',
      'bundle' => 'test_content_type',
      'required' => TRUE,
    ];
    FieldConfig::create($field)->save();
    // Assign form settings.
    \Drupal::service('entity_display.repository')->getFormDisplay('node', 'test_content_type')
      ->setComponent('field_address', [
        'type' => 'address_default',
      ])
      ->save();
    // Assign display settings.
    \Drupal::service('entity_display.repository')->getViewDisplay('node', 'test_content_type')
      ->setComponent('field_address', [
        'type' => 'address_default',
      ])
      ->save();
    // Create and login admin user.
    $this->adminUser = $this->drupalCreateUser([
      'access content overview',
      'administer nodes',
      'administer views',
      'create test_content_type content',
      'edit any test_content_type content',
    ]);
    $this->drupalLogin($this->adminUser);
    // Generate test contents.
    foreach ($this->getTestData() as $content) {
      $this->createNode($content + ['type' => 'test_content_type']);
    }
    ViewTestData::createTestViews(static::class, ['sbefa__test']);
  }
}
