<?php

declare(strict_types=1);

namespace Drupal\Tests\selective_better_exposed_filters_address\Functional;

use Drupal\Tests\BrowserTestBase;
use PHPUnit\Framework\Attributes\Group;

/**
 * "Filter address values based on filtered result set" filter setting test.
 */
#[Group('selective_better_exposed_filters_address')]
final class FilterAddressBasedOnFilteredResultTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static $testViews = ['sbefa__test'];

  /**
   * Test disabled filter.
   */
  public function testShowOnlyUsedAddress(): void {
    $this->drupalGet('/sbefa--test/filter');
    $this->assert
      ->elementsCount('css', 'select[name="field_address_country_code"] option', 5);
    $edit = [
      'field_address_country_code' => 'US'
    ];
    $this->submitForm($edit, 'Apply');
    $this->assert
      ->elementsCount('css', 'select[name="field_address_administrative_area"] option', 6);
    $edit = [
      'title' => 'Vero',
    ];
    $this->submitForm($edit, 'Apply');
    $this->assert
      ->elementsCount('css', 'select[name="field_address_country_code"] option', 2);
    $this->assert
      ->elementsCount('css', 'select[name="field_address_administrative_area"] option', 2);
  }
}
